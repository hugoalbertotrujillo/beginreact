import React from 'react';
import './App.css';

/*function HelloKoa(props) {
  return (
    <div id='hello'>
      <h3>{props.subtitle} </h3>
      {props.myText}
    </div>
  );
}
*/
class HelloKoa extends React.Component {
  
  state = {
    show: true
  }

  toggleShow = () => {
    /*this.setState({show: !this.state.show})*/
    if (this.state.show === true) {
      this.setState({ show: false })
    } else {
      this.setState({ show: true })
    }
  }
 

  render() {
    if (this.state.show) {
      return (
        <div id='hello'>
        <h3>{this.props.subtitle} </h3>
        {this.props.myText}
        {/*<button onClick={ () => this.setState({ show: false }) } >toogle show</button>*/}
        {/*<button onClick={ this.toggleShow.bind(this) EN CASO DE LLAMARLO COMO METODO: toggleShow(){...} } >toogle show</button>*/}
        <button onClick={ this.toggleShow } >toogle show</button>
      </div>
      );
    } else {
      return (
        <h3>
          no elemnts
          <button onClick={ this.toggleShow } >toogle show</button>
        </h3>
      );
    }
  }
}

function App() {
  return (
    <div>
      this is my component:
      <HelloKoa myText='one' subtitle='subOne' />
      <HelloKoa myText='two' subtitle='subTwo' />
      <HelloKoa myText='three' subtitle='subThree' />
    </div>
  );
}

export default App;