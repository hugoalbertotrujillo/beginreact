import React, { Component } from 'react';


class Task extends Component {
    render() {
        const {theTask} = this.props
        return (
            <div>
                {/*{this.props.theTask.title} - 
                {this.props.theTask.description} - 
                {this.props.theTask.done} - 
                {this.props.theTask.id}*/}
                {theTask.title} - 
                {theTask.description} - 
                {theTask.done} - 
                {theTask.id}
                <input type='checkbox' />
                <button>
                    x
                </button>
            </div>
        );
    }
}

export default Task;