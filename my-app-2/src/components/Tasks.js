import React, { Component } from 'react';
import Task from './Task'

class Tasks extends Component {
    render() {
        return (
            /*this.props.tasks.map(e => <p key={e.id} > 
                {e.title} - {e.description} - {e.done} - {e.id}
                </p>*/
            this.props.tasks.map(e => <Task theTask={e} key={e.id} />)
        );
    }
}

export default Tasks;