import React, { Component } from 'react'; /*asi no tengo q escribir React.Component en la clase*/
import './App.css';
import tasks from './sample/tasks.json'
import Tasks from './components/Tasks.js'

class App extends Component {
  
  state = {
    tasks: tasks
  }
  render() {
    return (
    <div>
        <Tasks tasks={this.state.tasks} />
      </div>
    );
  }
}

export default App;